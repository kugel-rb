<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="de">
<defaultcodec></defaultcodec>
<context>
    <name>BootloaderInstallBase</name>
    <message>
        <location filename="../base/bootloaderinstallbase.cpp" line="69"/>
        <source>Download error: received HTTP error %1.</source>
        <translation>Fehler beim Herunterladen: HTTP Fehler %1.</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallbase.cpp" line="75"/>
        <source>Download error: %1</source>
        <translation>Fehler beim Herunterladen: %1</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallbase.cpp" line="80"/>
        <source>Download finished (cache used).</source>
        <translation>Download abgeschlossen (Cache verwendet).</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallbase.cpp" line="82"/>
        <source>Download finished.</source>
        <translation>Download abgeschlossen.</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallbase.cpp" line="103"/>
        <source>Creating backup of original firmware file.</source>
        <translation>Erzeuge Sicherungskopie der Original-Firmware.</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallbase.cpp" line="105"/>
        <source>Creating backup folder failed</source>
        <translation>Erzeugen des Sicherungskopie-Ordners fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallbase.cpp" line="111"/>
        <source>Creating backup copy failed.</source>
        <translation>Erzeugen der Sicherungskopie fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallbase.cpp" line="114"/>
        <source>Backup created.</source>
        <translation>Sicherungskopie erzeugt.</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallbase.cpp" line="127"/>
        <source>Creating installation log</source>
        <translation>Erzeuge Installationslog</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallbase.cpp" line="152"/>
        <source>Bootloader installation is almost complete. Installation &lt;b&gt;requires&lt;/b&gt; you to perform the following steps manually:</source>
        <translation>Installation des Bootloader ist fast abgeschlossen. Die Installation &lt;b&gt;benötigt&lt;/b&gt; die folgenden, manuell auszuführenden Schritte:</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallbase.cpp" line="155"/>
        <source>&lt;li&gt;Safely remove your player.&lt;/li&gt;</source>
        <translation>&lt;li&gt;Gerät sicher entfernen.&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallbase.cpp" line="162"/>
        <source>&lt;li&gt;Reboot your player into the original firmware.&lt;/li&gt;&lt;li&gt;Perform a firmware upgrade using the update functionality of the original firmware. Please refer to your player&apos;s manual on details.&lt;/li&gt;&lt;li&gt;After the firmware has been updated reboot your player.&lt;/li&gt;</source>
        <translation>&lt;li&gt;Gerät in die Original-Firmware booten.&lt;/li&gt;&lt;li&gt;Firmware-Upgrade mit der Upgrade-Funktionalität der Original-Firmware durchführen. Für Details bitte das Handbuch des Geräteherstellers beachten.&lt;/li&gt;&lt;li&gt;Nachdem die Firmware aktualisiert wurde das Gerät neu starten.&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallbase.cpp" line="168"/>
        <source>&lt;li&gt;Turn the player off&lt;/li&gt;&lt;li&gt;Insert the charger&lt;/li&gt;</source>
        <translation>&lt;li&gt;Gerät ausschalten&lt;/li&gt;&lt;li&gt;Ladegerät anstecken&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallbase.cpp" line="175"/>
        <source>&lt;li&gt;Unplug USB and power adaptors&lt;/li&gt;&lt;li&gt;Hold &lt;i&gt;Power&lt;/i&gt; to turn the player off&lt;/li&gt;&lt;li&gt;Toggle the battery switch on the player&lt;/li&gt;&lt;li&gt;Hold &lt;i&gt;Power&lt;/i&gt; to boot into Rockbox&lt;/li&gt;</source>
        <translation>&lt;li&gt;USB und Stromkabel abziehen&lt;/li&gt;&lt;li&gt;&lt;i&gt;Power&lt;/i&gt; gedrückt halten um das Gerät auszuschalten&lt;/li&gt;&lt;li&gt;Batterieschalter am Gerät umlegen&lt;/li&gt;&lt;li&gt;&lt;i&gt;Power&lt;/i&gt; halten um Rockbox zu booten&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallbase.cpp" line="180"/>
        <source>&lt;p&gt;&lt;b&gt;Note:&lt;/b&gt; You can safely install other parts first, but the above steps are &lt;b&gt;required&lt;/b&gt; to finish the installation!&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Hinweis:&lt;/b&gt; andere Teile von Rockbox können problemlos vorher installiert werden, aber die genannten Schritte sind &lt;b&gt;notwendig&lt;/b&gt; um die Installation abzuschließen!&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallbase.cpp" line="138"/>
        <source>Installation log created</source>
        <translation>Installationslog erzeugt</translation>
    </message>
</context>
<context>
    <name>BootloaderInstallFile</name>
    <message>
        <location filename="../base/bootloaderinstallfile.cpp" line="35"/>
        <source>Downloading bootloader</source>
        <translation>Lade Bootloader herunter</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallfile.cpp" line="44"/>
        <source>Installing Rockbox bootloader</source>
        <translation>Installiere Rockbox Bootloader</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallfile.cpp" line="76"/>
        <source>Error accessing output folder</source>
        <translation>Fehler beim Zugriff auf den Ausgabeordner</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallfile.cpp" line="89"/>
        <source>Bootloader successful installed</source>
        <translation>Bootloader erfolgreich installiert</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallfile.cpp" line="99"/>
        <source>Removing Rockbox bootloader</source>
        <translation>Entferne Rockbox Bootloader</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallfile.cpp" line="103"/>
        <source>No original firmware file found.</source>
        <translation>Keine Original-Firmware gefunden.</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallfile.cpp" line="109"/>
        <source>Can&apos;t remove Rockbox bootloader file.</source>
        <translation>Kann Rockbox Bootloader-Datei nicht entfernen.</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallfile.cpp" line="114"/>
        <source>Can&apos;t restore bootloader file.</source>
        <translation>Kann Bootloader-Datei nicht wiederherstellen.</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallfile.cpp" line="118"/>
        <source>Original bootloader restored successfully.</source>
        <translation>Original-Bootloader erfolgreich wiederhergestellt.</translation>
    </message>
</context>
<context>
    <name>BootloaderInstallHex</name>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="68"/>
        <source>checking MD5 hash of input file ...</source>
        <translation>prüfe MD5-Hash der Eingabedatei ...</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="79"/>
        <source>Could not verify original firmware file</source>
        <translation>Konnte Originalfirmware-Datei nicht prüfen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="94"/>
        <source>Firmware file not recognized.</source>
        <translation>Firmware-Datei nicht erkannt.</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="98"/>
        <source>MD5 hash ok</source>
        <translation>MD5-Hash ok</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="105"/>
        <source>Firmware file doesn&apos;t match selected player.</source>
        <translation>Firmware passt nicht zum gewählten Gerät.</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="110"/>
        <source>Descrambling file</source>
        <translation>Descramble Datei</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="118"/>
        <source>Error in descramble: %1</source>
        <translation>Fehler bei Descramble: %1</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="123"/>
        <source>Downloading bootloader file</source>
        <translation>Lade Bootloader-Datei herunter</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="133"/>
        <source>Adding bootloader to firmware file</source>
        <translation>Füge Bootloader zu Firmware-Datei hinzu</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="171"/>
        <source>could not open input file</source>
        <translation>Konnte die Eingabedatei nicht öffnen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="172"/>
        <source>reading header failed</source>
        <translation>Konnte Header nicht lesen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="173"/>
        <source>reading firmware failed</source>
        <translation>Konnte Firmware nicht lesen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="174"/>
        <source>can&apos;t open bootloader file</source>
        <translation>Konnte Bootloader nicht öffnen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="175"/>
        <source>reading bootloader file failed</source>
        <translation>Konnte Bootloader nicht lesen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="176"/>
        <source>can&apos;t open output file</source>
        <translation>Konnte Ausgabedatei nicht öffnen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="177"/>
        <source>writing output file failed</source>
        <translation>Konnte Ausgabedatei nicht schreiben</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="179"/>
        <source>Error in patching: %1</source>
        <translation>Fehler beim Patchen %1</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="190"/>
        <source>Error in scramble: %1</source>
        <translation>Fehler bei Scramble: %1</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="205"/>
        <source>Checking modified firmware file</source>
        <translation>Prüfe modifizierte Firmware-Datei</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="207"/>
        <source>Error: modified file checksum wrong</source>
        <translation>Fehler: Prüfsumme der modifizierten Datei falsch</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="215"/>
        <source>Success: modified firmware file created</source>
        <translation>Erfolg: modifizierte Firmware-Datei erzeugt</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="246"/>
        <source>Can&apos;t open input file</source>
        <translation>Konnte Eingabedatei nicht öffnen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="247"/>
        <source>Can&apos;t open output file</source>
        <translation>Konnte Ausgabedatei nicht öffnen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="248"/>
        <source>invalid file: header length wrong</source>
        <translation>ungültige Datei: Länge des Headers ist falsch</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="249"/>
        <source>invalid file: unrecognized header</source>
        <translation>ungültige Datei: unbekannter Header</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="250"/>
        <source>invalid file: &quot;length&quot; field wrong</source>
        <translation>ungültige Datei: &quot;length&quot; Eintrag ist falsch</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="251"/>
        <source>invalid file: &quot;length2&quot; field wrong</source>
        <translation>ungültige Datei: &quot;length2&quot; Eintrag ist falsch</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="252"/>
        <source>invalid file: internal checksum error</source>
        <translation>ungültige Datei: interne Prüfsumme ist falsch</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="253"/>
        <source>invalid file: &quot;length3&quot; field wrong</source>
        <translation>ungültige Datei: &quot;length3&quot; Eintrag ist falsch</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="254"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallhex.cpp" line="58"/>
        <source>Bootloader installation requires you to provide a firmware file of the original firmware (hex file). You need to download this file yourself due to legal reasons. Please refer to the &lt;a href=&apos;http://www.rockbox.org/manual.shtml&apos;&gt;manual&lt;/a&gt; and the &lt;a href=&apos;http://www.rockbox.org/wiki/IriverBoot#Download_and_extract_a_recent_ve&apos;&gt;IriverBoot&lt;/a&gt; wiki page on how to obtain this file.&lt;br/&gt;Press Ok to continue and browse your computer for the firmware file.</source>
        <translation type="unfinished">Die Bootloader-Installation benötigt eine Firmware-Datei der originalen Firmware (Hex-Datei). Diese Datei muss aus rechtlichen Gründen separat heruntergeladen werden. Informationen wie diese Datei heruntergeladen werden kann sind im &lt;a href=&apos;http://www.rockbox.org/manual.shtml&apos;&gt;Handbuch&lt;/a&gt; und der Wiki-Seite &lt;a href=&apos;http://www.rockbox.org/wiki/IriverBoot#Download_and_extract_a_recent_ve&apos;&gt;IriverBoot&lt;/a&gt; aufgeführt.&lt;br/&gt;OK um fortfahren und die Datei auf dem Computer auszuwählen.</translation>
    </message>
</context>
<context>
    <name>BootloaderInstallIpod</name>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="54"/>
        <source>Error: can&apos;t allocate buffer memory!</source>
        <translation>Fehler: kann Speicher nicht allokieren!</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="64"/>
        <source>No Ipod detected
Permission for disc access denied!</source>
        <translation>Kein Ipod gefunden
Laufwerkszugriff verweigert!</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="70"/>
        <source>No Ipod detected!</source>
        <translation>Kein Ipod gefunden!</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="81"/>
        <source>Downloading bootloader file</source>
        <translation>Lade Bootloader-Datei herunter</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="161"/>
        <source>Failed to read firmware directory</source>
        <translation>Konnte Firmwareverzeichnis nicht lesen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="166"/>
        <source>Unknown version number in firmware (%1)</source>
        <translation>Unbekannte Versionsnummer in Firmware (%1)</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="173"/>
        <source>Could not open Ipod in R/W mode</source>
        <translation>Kann Ipod nicht im R/W-Modus öffnen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="131"/>
        <source>Successfull added bootloader</source>
        <translation>Bootloader erfolgreich hinzugefügt</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="138"/>
        <source>Failed to add bootloader</source>
        <translation>Konnte Bootloader nicht hinzufügen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="179"/>
        <source>No bootloader detected.</source>
        <translation>Kein Bootloader erkannt.</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="252"/>
        <source>Could not open Ipod</source>
        <translation>Konnte Ipod nicht öffnen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="263"/>
        <source>No firmware partition on disk</source>
        <translation>Keine Firmware-Partition auf dem Laufwerk</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="116"/>
        <source>Warning: This is a MacPod, Rockbox only runs on WinPods.
See http://www.rockbox.org/wiki/IpodConversionToFAT32</source>
        <translation>Warnung: Dies ist ein MacPod, Rockbox läuft nur auf WinPods.
Siehe http://www.rockbox.org/wiki/IpodConversionToFAT32</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="185"/>
        <source>Successfully removed bootloader</source>
        <translation>Bootloader erfolgreich entfernt</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="192"/>
        <source>Removing bootloader failed.</source>
        <translation>Entfernen des Bootloaders fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="93"/>
        <source>Installing Rockbox bootloader</source>
        <translation type="unfinished">Installiere Rockbox Bootloader</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="150"/>
        <source>Uninstalling bootloader</source>
        <translation>Entferne Bootloader</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallipod.cpp" line="257"/>
        <source>Error reading partition table - possibly not an Ipod</source>
        <translation>Fehler beim Lesen der Partitionstabelle - möglicherweise kein Ipod</translation>
    </message>
</context>
<context>
    <name>BootloaderInstallMi4</name>
    <message>
        <location filename="../base/bootloaderinstallmi4.cpp" line="34"/>
        <source>Downloading bootloader</source>
        <translation>Lade Bootloader herunter</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallmi4.cpp" line="43"/>
        <source>Installing Rockbox bootloader</source>
        <translation>Installiere Rockbox Bootloader</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallmi4.cpp" line="66"/>
        <source>Bootloader successful installed</source>
        <translation>Bootloader erfolgreich installiert</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallmi4.cpp" line="78"/>
        <source>Checking for Rockbox bootloader</source>
        <translation>Prüfe auf Rockbox-Bootloader</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallmi4.cpp" line="80"/>
        <source>No Rockbox bootloader found</source>
        <translation>Kein Rockbox-Bootloader gefunden</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallmi4.cpp" line="85"/>
        <source>Checking for original firmware file</source>
        <translation>Prüfe auf Firmwaredatei der Originalfirmware</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallmi4.cpp" line="90"/>
        <source>Error finding original firmware file</source>
        <translation>Fehler beim finden der Originalfirmware-Datei</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallmi4.cpp" line="101"/>
        <source>Rockbox bootloader successful removed</source>
        <translation>Rockbox Bootloader erfolgreich entfernt</translation>
    </message>
</context>
<context>
    <name>BootloaderInstallSansa</name>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="56"/>
        <source>Error: can&apos;t allocate buffer memory!</source>
        <translation>Fehler: kann Speicher nicht allokieren!</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="61"/>
        <source>Searching for Sansa</source>
        <translation>Suche nach Sansa</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="68"/>
        <source>Permission for disc access denied!
This is required to install the bootloader</source>
        <translation>Direkter Laufwerkszugriff verweigert!
Der Zugriff ist notwendig um den Bootloader zu installieren</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="74"/>
        <source>No Sansa detected!</source>
        <translation>Kein Sansa gefunden!</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="78"/>
        <source>Downloading bootloader file</source>
        <translation>Lade Bootloader-Datei herunter</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="171"/>
        <source>OLD ROCKBOX INSTALLATION DETECTED, ABORTING.
You must reinstall the original Sansa firmware before running
sansapatcher for the first time.
See http://www.rockbox.org/wiki/SansaE200Install
</source>
        <translation>ALTE ROCKBOX-INSTALLATION ERKANNT, ABBRUCH.
Die Original-Sansa-Firmware muss neu installiert werden
bevor sansapatcher zum ersten Mal verwendet werden kann.
Siehe http://www.rockbox.org/wiki/SansaE200Install
</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="178"/>
        <source>Could not open Sansa in R/W mode</source>
        <translation>Konnte Sansa nicht im R/W-Modus öffnen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="137"/>
        <source>Successfully installed bootloader</source>
        <translation>Bootloader erfolgreich installiert</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="144"/>
        <source>Failed to install bootloader</source>
        <translation>Bootloader-Installation fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="236"/>
        <source>Can&apos;t find Sansa</source>
        <translation>Konnte Sansa nicht finden</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="241"/>
        <source>Could not open Sansa</source>
        <translation>Konnte Sansa nicht öffnen </translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="246"/>
        <source>Could not read partition table</source>
        <translation>Konnte Partitionstabelle nicht lesen</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="253"/>
        <source>Disk is not a Sansa (Error %1), aborting.</source>
        <translation>Laufwerk ist kein Sansa (Fehler: %1), breche ab.</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="184"/>
        <source>Successfully removed bootloader</source>
        <translation>Bootloader erfolgreich entfernt</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="191"/>
        <source>Removing bootloader failed.</source>
        <translation>Entfernen des Bootloaders fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="92"/>
        <source>Installing Rockbox bootloader</source>
        <translation type="unfinished">Installiere Rockbox Bootloader</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="159"/>
        <source>Uninstalling bootloader</source>
        <translation>Entferne Bootloader</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="119"/>
        <source>Checking downloaded bootloader</source>
        <translation type="unfinished">Prüfe heruntergeladenen Bootloader</translation>
    </message>
    <message>
        <location filename="../base/bootloaderinstallsansa.cpp" line="127"/>
        <source>Bootloader mismatch! Aborting.</source>
        <translation type="unfinished">Fehler im Bootloader! Abbruch.</translation>
    </message>
</context>
<context>
    <name>BrowseDirtreeFrm</name>
    <message>
        <location filename="../browsedirtreefrm.ui" line="13"/>
        <source>Find Directory</source>
        <translation>Suche Verzeichnis</translation>
    </message>
    <message>
        <location filename="../browsedirtreefrm.ui" line="19"/>
        <source>Browse to the destination folder</source>
        <translation>Suche Zielordner</translation>
    </message>
    <message>
        <location filename="../browsedirtreefrm.ui" line="47"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../browsedirtreefrm.ui" line="57"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
</context>
<context>
    <name>Config</name>
    <message>
        <location filename="../configure.cpp" line="119"/>
        <source>Language changed</source>
        <translation>Sprache geändert</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="120"/>
        <source>You need to restart the application for the changed language to take effect.</source>
        <translation>Die Anwendung muss neu gestartet werden um die geänderten Spracheinstallungen anzuwenden.</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="617"/>
        <source>Autodetection</source>
        <translation>Automatische Erkennung</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="610"/>
        <source>Could not detect a Mountpoint.
Select your Mountpoint manually.</source>
        <translation>Konnte Einhängepunkt nicht erkennen.
Bitte manuell auswählen.</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="619"/>
        <source>Could not detect a device.
Select your device and Mountpoint manually.</source>
        <translation>Konnte kein Gerät erkennen.
Bitte Gerät und Einhängepunt manuell auswählen.</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="628"/>
        <source>Really delete cache?</source>
        <translation>Cache wirklich löschen?</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="631"/>
        <source>Do you really want to delete the cache? Make absolutely sure this setting is correct as it will remove &lt;b&gt;all&lt;/b&gt; files in this folder!</source>
        <translation>Cache wirklich löschen? Unbedingt sicherstellen dass die Enstellungen korrekt sind, dies löscht &lt;b&gt;alle&lt;/b&gt; Dateien im Cache-Ordner!</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="637"/>
        <source>Path wrong!</source>
        <translation>Pfad fehlerhaft!</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="638"/>
        <source>The cache path is invalid. Aborting.</source>
        <translation>Cache-Pfad ist ungültig. Abbruch.</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="265"/>
        <source>Current cache size is %L1 kiB.</source>
        <translation>Aktuelle Cachegröße ist %L1 kiB. </translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="488"/>
        <source>Select your device</source>
        <translation>Gerät auswählen</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="579"/>
        <source>Sansa e200 in MTP mode found!
You need to change your player to MSC mode for installation. </source>
        <translation>Sansa e200 in MTP Modus gefunden!
Das Gerät muss für die Installation in den MSC-Modus umgestellt werden.</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="582"/>
        <source>H10 20GB in MTP mode found!
You need to change your player to UMS mode for installation. </source>
        <translation>H10 20GB in MTP Modus gefunden!
Das Gerät muss für die Installation in den UMS-Modus umgestellt werden.</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="583"/>
        <source>Unless you changed this installation will fail!</source>
        <translation>Die Installation wird fehlschlagen bis dies korrigiert ist!</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="585"/>
        <source>Fatal error</source>
        <translation>Fataler Fehler</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="595"/>
        <source>Detected an unsupported %1 player variant. Sorry, Rockbox doesn&apos;t run on your player.</source>
        <translation>Nicht unterstützte %1 Gerätevariante erkannt. Rockbox funktioniert leider nicht auf diesem Gerät.</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="597"/>
        <source>Fatal error: incompatible player found</source>
        <translation>Fataler Fehler: inkompatibles Gerät gefunden </translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="384"/>
        <source>Configuration OK</source>
        <translation>Konfiguration OK</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="389"/>
        <source>Configuration INVALID</source>
        <translation>Konfiguration UNGÜLTIG</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="96"/>
        <source>The following errors occurred:</source>
        <translation>Die folgenden Fehler sind aufgetreten:</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="127"/>
        <source>No mountpoint given</source>
        <translation>Kein Einhängepunkt ausgewählt</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="131"/>
        <source>Mountpoint does not exist</source>
        <translation>Einhängepunkt existiert nicht</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="135"/>
        <source>Mountpoint is not a directory.</source>
        <translation>Einhängepunkt ist kein Verzeichnis</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="139"/>
        <source>Mountpoint is not writeable</source>
        <translation>Einhängepunkt ist nicht schreibbar</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="153"/>
        <source>No player selected</source>
        <translation>Kein Gerät ausgewählt</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="161"/>
        <source>Cache path not writeable. Leave path empty to default to systems temporary path.</source>
        <translation>Cache-Pfad ist nicht schreibbar. Um auf den temporären Pfad des Systems zurückzusetzen den Pfad leer lassen.</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="179"/>
        <source>You need to fix the above errors before you can continue.</source>
        <translation>Die Fehler müssen beseitigt werden um fortzufahren.</translation>
    </message>
    <message>
        <location filename="../configure.cpp" line="182"/>
        <source>Configuration error</source>
        <translation>Konfigurationsfehler</translation>
    </message>
</context>
<context>
    <name>ConfigForm</name>
    <message>
        <location filename="../configurefrm.ui" line="13"/>
        <source>Configuration</source>
        <translation>Konfiguration</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="19"/>
        <source>Configure Rockbox Utility</source>
        <translation>Rockbox Utility konfigurieren</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="501"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="512"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="108"/>
        <source>&amp;Proxy</source>
        <translation>&amp;Proxy</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="118"/>
        <source>&amp;No Proxy</source>
        <translation>&amp;kein Proxy</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="135"/>
        <source>&amp;Manual Proxy settings</source>
        <translation>&amp;Manuelle Proxyeinstellungen</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="142"/>
        <source>Proxy Values</source>
        <translation>Proxy-Einstellungen</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="148"/>
        <source>&amp;Host:</source>
        <translation>&amp;Host:</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="165"/>
        <source>&amp;Port:</source>
        <translation>&amp;Port:</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="188"/>
        <source>&amp;Username</source>
        <translation>&amp;Benutzername</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="225"/>
        <source>&amp;Language</source>
        <translation>&amp;Sprache</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="30"/>
        <source>&amp;Device</source>
        <translation>&amp;Gerät</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="40"/>
        <source>Select your device in the &amp;filesystem</source>
        <translation>Gerät im &amp;Dateisystem auswählen</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="288"/>
        <source>&amp;Browse</source>
        <translation>D&amp;urchsuchen</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="71"/>
        <source>&amp;Select your audio player</source>
        <translation>Gerät au&amp;swählen</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="82"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="90"/>
        <source>&amp;Autodetect</source>
        <translation>&amp;automatische Erkennung</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="128"/>
        <source>Use S&amp;ystem values</source>
        <translation>S&amp;ystemwerte verwenden</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="198"/>
        <source>Pass&amp;word</source>
        <translation>Pass&amp;wort</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="239"/>
        <source>Cac&amp;he</source>
        <translation>Cac&amp;he</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="246"/>
        <source>Download cache settings</source>
        <translation>Downloadcache-Einstellungen</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="252"/>
        <source>Rockbox Utility uses a local download cache to save network traffic. You can change the path to the cache and use it as local repository by enabling Offline mode.</source>
        <translation>Rockbox Utility verwendet einen lokalen Download-Cache um die übertragene Datenmenge zu begrenzen. Der Pfad zum Cache kann geändert sowie im Offline-Modus als lokales Repository verwenden werden.</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="262"/>
        <source>Current cache size is %1</source>
        <translation>Aktuelle Cache-Größe ist %1</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="271"/>
        <source>P&amp;ath</source>
        <translation>P&amp;fad</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="303"/>
        <source>Disable local &amp;download cache</source>
        <translation>lokalen &amp;Download-Cache ausschalten</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="313"/>
        <source>O&amp;ffline mode</source>
        <translation>O&amp;ffline-Modus</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="348"/>
        <source>Clean cache &amp;now</source>
        <translation>C&amp;ache löschen</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="281"/>
        <source>Entering an invalid folder will reset the path to the systems temporary path.</source>
        <translation>Ein ungültiger Ordner setzt den Pfad auf den temporären Pfad des Systems zurück.</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="310"/>
        <source>This will try to use all information from the cache, even information about updates. Only use this option if you want to install without network connection. Note: you need to do the same install you want to perform later with network access first to download all required files to the cache.</source>
        <translation>Dies versucht alle Informationen aus dem Cache zu beziehen, selbst die Informationen über Updates. Diese Option nur benutzen um ohne Internetverbindung zu installieren. Hinweis: die gleiche Installation, die später durchgeführt werden soll, muss einmal mit Netzwerkverbindung durchführt werden, damit die notwendigen Dateien im Cache gespeichert sind.</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="360"/>
        <source>&amp;TTS &amp;&amp; Encoder</source>
        <translation>&amp;TTS &amp;&amp; Encoder</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="370"/>
        <source>TTS Engine</source>
        <translation>TTS-System</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="427"/>
        <source>Encoder Engine</source>
        <translation>Encoder-System</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="376"/>
        <source>&amp;Select TTS Engine</source>
        <translation>TT&amp;S-System auswählen</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="389"/>
        <source>Configure TTS Engine</source>
        <translation>TTS-System konfigurieren</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="433"/>
        <source>Configuration invalid!</source>
        <translation>Konfiguration ungültig!</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="413"/>
        <source>Configure &amp;TTS</source>
        <translation>&amp;TTS konfigurieren</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="450"/>
        <source>Configure &amp;Enc</source>
        <translation> &amp;Encoder konfigurieren</translation>
    </message>
    <message>
        <location filename="../configurefrm.ui" line="461"/>
        <source>encoder name</source>
        <translation>Encoder-Name</translation>
    </message>
</context>
<context>
    <name>Configure</name>
    <message>
        <location filename="../configure.cpp" line="472"/>
        <source>English</source>
        <comment>This is the localized language name, i.e. your language.</comment>
        <translation>Deutsch</translation>
    </message>
</context>
<context>
    <name>CreateVoiceFrm</name>
    <message>
        <location filename="../createvoicefrm.ui" line="16"/>
        <source>Create Voice File</source>
        <translation>Sprachdatei erstellen</translation>
    </message>
    <message>
        <location filename="../createvoicefrm.ui" line="41"/>
        <source>Select the Language you want to generate a voicefile for:</source>
        <translation>Sprache auswählen, für die die Sprachdatei generiert werden soll:</translation>
    </message>
    <message>
        <location filename="../createvoicefrm.ui" line="51"/>
        <source>Generation settings</source>
        <translation>Allgemeine Einstellungen</translation>
    </message>
    <message>
        <location filename="../createvoicefrm.ui" line="57"/>
        <source>Encoder profile:</source>
        <translation>Encoder-Profil:</translation>
    </message>
    <message>
        <location filename="../createvoicefrm.ui" line="64"/>
        <source>TTS profile:</source>
        <translation>TTS-Profil:</translation>
    </message>
    <message>
        <location filename="../createvoicefrm.ui" line="77"/>
        <source>Change</source>
        <translation>Ändern</translation>
    </message>
    <message>
        <location filename="../createvoicefrm.ui" line="128"/>
        <source>&amp;Install</source>
        <translation>&amp;Installieren</translation>
    </message>
    <message>
        <location filename="../createvoicefrm.ui" line="138"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../createvoicefrm.ui" line="152"/>
        <source>Wavtrim Threshold</source>
        <translation>Wavtrim Schwellenwert</translation>
    </message>
</context>
<context>
    <name>CreateVoiceWindow</name>
    <message>
        <location filename="../createvoicewindow.cpp" line="97"/>
        <source>Selected TTS engine: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Gewähltes TTS-System: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../createvoicewindow.cpp" line="112"/>
        <source>Selected encoder: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Gewählter Encoder: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>EncExes</name>
    <message>
        <location filename="../base/encoders.cpp" line="95"/>
        <source>Path to Encoder:</source>
        <translation>Pfad zum Encoder:</translation>
    </message>
    <message>
        <location filename="../base/encoders.cpp" line="97"/>
        <source>Encoder options:</source>
        <translation>Encoder Optionen:</translation>
    </message>
</context>
<context>
    <name>EncRbSpeex</name>
    <message>
        <location filename="../base/encoders.cpp" line="161"/>
        <source>Volume:</source>
        <translation>Lautstärke:</translation>
    </message>
    <message>
        <location filename="../base/encoders.cpp" line="163"/>
        <source>Quality:</source>
        <translation>Qualität:</translation>
    </message>
    <message>
        <location filename="../base/encoders.cpp" line="165"/>
        <source>Complexity:</source>
        <translation>Komplexität:</translation>
    </message>
    <message>
        <location filename="../base/encoders.cpp" line="167"/>
        <source>Use Narrowband:</source>
        <translation>Benutze Schmalband:</translation>
    </message>
</context>
<context>
    <name>EncTtsCfgGui</name>
    <message>
        <location filename="../encttscfggui.cpp" line="32"/>
        <source>Waiting for engine...</source>
        <translation type="unfinished">Warte auf Engine ...</translation>
    </message>
    <message>
        <location filename="../encttscfggui.cpp" line="67"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../encttscfggui.cpp" line="70"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../encttscfggui.cpp" line="173"/>
        <source>Browse</source>
        <translation>Durchsuchen</translation>
    </message>
    <message>
        <location filename="../encttscfggui.cpp" line="181"/>
        <source>Refresh</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <location filename="../encttscfggui.cpp" line="353"/>
        <source>Select excutable</source>
        <translation>Programm auswählen</translation>
    </message>
</context>
<context>
    <name>Install</name>
    <message>
        <location filename="../install.cpp" line="98"/>
        <source>Mount point is wrong!</source>
        <translation>Falscher Einhängepunkt!</translation>
    </message>
    <message>
        <location filename="../install.cpp" line="252"/>
        <source>This is the last released version of Rockbox.</source>
        <translation>Dies ist die letzte veröffentlichte Version von Rockbox.</translation>
    </message>
    <message>
        <location filename="../install.cpp" line="239"/>
        <source>This is the absolute up to the minute Rockbox built. A current build will get updated every time a change is made. Latest version is r%1 (%2).</source>
        <translation type="unfinished">Dies ist das aktuellste Rockbox build. Es wird bei jeder Änderung aktualisiert. Letzte Version ist r%1 (%2).</translation>
    </message>
    <message>
        <location filename="../install.cpp" line="272"/>
        <source>&lt;b&gt;Note:&lt;/b&gt; archived version is r%1 (%2).</source>
        <translation>&lt;b&gt;Hinweis:&lt;/b&gt; Archivierte Version ist r%1 (%2).</translation>
    </message>
    <message>
        <location filename="../install.cpp" line="270"/>
        <source>These are automatically built each day from the current development source code. This generally has more features than the last stable release but may be much less stable. Features may change regularly.</source>
        <translation type="unfinished">Diese Builds werden jeden Tag automatisch aus dem aktuellen Source Code gebaut. Sie haben meist mehr Features als das letzte stabile Release, können aber weniger stabil sein. Features können sich regelmäßig ändern.</translation>
    </message>
    <message>
        <location filename="../install.cpp" line="143"/>
        <source>Aborted!</source>
        <translation>Abgebrochen!</translation>
    </message>
    <message>
        <location filename="../install.cpp" line="152"/>
        <source>Beginning Backup...</source>
        <translation>Erstelle Sicherungskopie ...</translation>
    </message>
    <message>
        <location filename="../install.cpp" line="168"/>
        <source>Backup successful</source>
        <translation>Sicherung erfolgreich</translation>
    </message>
    <message>
        <location filename="../install.cpp" line="172"/>
        <source>Backup failed!</source>
        <translation>Sicherung fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="../install.cpp" line="139"/>
        <source>Really continue?</source>
        <translation>Wirklich fortfahren?</translation>
    </message>
    <message>
        <location filename="../install.cpp" line="68"/>
        <source>Backup to %1</source>
        <translation>Sicherungskopie nach %1</translation>
    </message>
    <message>
        <location filename="../install.cpp" line="203"/>
        <source>Select Backup Filename</source>
        <translation>Dateiname für Sicherungskopie auswählen</translation>
    </message>
    <message>
        <location filename="../install.cpp" line="241"/>
        <source>&lt;b&gt;This is the recommended version.&lt;/b&gt;</source>
        <translation>&lt;b&gt;Dies ist die empfohlene Version.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../install.cpp" line="258"/>
        <source>&lt;b&gt;Note:&lt;/b&gt; The lastest released version is %1. &lt;b&gt;This is the recommended version.&lt;/b&gt;</source>
        <translation type="unfinished">&lt;b&gt;Hinweis:&lt;/b&gt; Die letzte Release-Version ist %1. &lt;b&gt;Dies ist die empfohlene Version.&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>InstallFrm</name>
    <message>
        <location filename="../installfrm.ui" line="16"/>
        <source>Install Rockbox</source>
        <translation>Rockbox installieren</translation>
    </message>
    <message>
        <location filename="../installfrm.ui" line="35"/>
        <source>Please select the Rockbox version you want to install on your player:</source>
        <translation>Bitte die zu installierende Version von Rockbox auswählen:</translation>
    </message>
    <message>
        <location filename="../installfrm.ui" line="45"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../installfrm.ui" line="51"/>
        <source>Rockbox &amp;stable</source>
        <translation>&amp;Stabile Rockbox-Version</translation>
    </message>
    <message>
        <location filename="../installfrm.ui" line="58"/>
        <source>&amp;Archived Build</source>
        <translation>&amp;Archivierte Version</translation>
    </message>
    <message>
        <location filename="../installfrm.ui" line="65"/>
        <source>&amp;Current Build</source>
        <translation>Aktuelle &amp;Version</translation>
    </message>
    <message>
        <location filename="../installfrm.ui" line="75"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../installfrm.ui" line="81"/>
        <source>Details about the selected version</source>
        <translation>Details über die ausgewählte Version</translation>
    </message>
    <message>
        <location filename="../installfrm.ui" line="91"/>
        <source>Note</source>
        <translation>Hinweis</translation>
    </message>
    <message>
        <location filename="../installfrm.ui" line="198"/>
        <source>Rockbox Utility stores copies of Rockbox it has downloaded on the local hard disk to save network traffic. If your local copy is no longer working, tick this box to download a fresh copy.</source>
        <translation>Rockbox Utility speichert bereits heruntergeladenen Kopien von Rockbox auf der lokalen Festplatte um den Netzwerkverkehr zu begrenzen. Wenn die lokale Kopie nicht weiter funktioniert, diese Option verwenden um eine neue Kopie herunterzuladen.</translation>
    </message>
    <message>
        <location filename="../installfrm.ui" line="201"/>
        <source>&amp;Don&apos;t use locally cached copy</source>
        <translation>&amp;keine lokale Zwischenkopie verwenden</translation>
    </message>
    <message>
        <location filename="../installfrm.ui" line="130"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../installfrm.ui" line="119"/>
        <source>&amp;Install</source>
        <translation>&amp;Installiere</translation>
    </message>
    <message>
        <location filename="../installfrm.ui" line="156"/>
        <source>Backup</source>
        <translation>Sicherungskopie</translation>
    </message>
    <message>
        <location filename="../installfrm.ui" line="162"/>
        <source>Backup before installing</source>
        <translation>Erstelle Sicherungskopie vor der Installation</translation>
    </message>
    <message>
        <location filename="../installfrm.ui" line="169"/>
        <source>Backup location</source>
        <translation>Speicherort für Sicherungskopie</translation>
    </message>
    <message>
        <location filename="../installfrm.ui" line="188"/>
        <source>Change</source>
        <translation>Ändern</translation>
    </message>
</context>
<context>
    <name>InstallTalkFrm</name>
    <message>
        <location filename="../installtalkfrm.ui" line="16"/>
        <source>Install Talk Files</source>
        <translation>Talk-Dateien installieren</translation>
    </message>
    <message>
        <location filename="../installtalkfrm.ui" line="35"/>
        <source>Select the Folder to generate Talkfiles for.</source>
        <translation>Ordner, für den Talk-Dateien erstellt werden sollen, auswählen.</translation>
    </message>
    <message>
        <location filename="../installtalkfrm.ui" line="45"/>
        <source>&amp;Browse</source>
        <translation>&amp;Durchsuchen</translation>
    </message>
    <message>
        <location filename="../installtalkfrm.ui" line="111"/>
        <source>Run recursive</source>
        <translation>Rekursiv durchlaufen</translation>
    </message>
    <message>
        <location filename="../installtalkfrm.ui" line="131"/>
        <source>Overwrite Talkfiles</source>
        <translation>Talk-Dateien überschreiben</translation>
    </message>
    <message>
        <location filename="../installtalkfrm.ui" line="121"/>
        <source>Strip Extensions</source>
        <translation>Dateiendungen entfernen</translation>
    </message>
    <message>
        <location filename="../installtalkfrm.ui" line="213"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../installtalkfrm.ui" line="56"/>
        <source>Generation settings</source>
        <translation>Allgemeine Einstellungen</translation>
    </message>
    <message>
        <location filename="../installtalkfrm.ui" line="62"/>
        <source>Encoder profile:</source>
        <translation>Encoder-Profil:</translation>
    </message>
    <message>
        <location filename="../installtalkfrm.ui" line="69"/>
        <source>TTS profile:</source>
        <translation>TTS-Profil:</translation>
    </message>
    <message>
        <location filename="../installtalkfrm.ui" line="105"/>
        <source>Generation options</source>
        <translation>Generierungsoptionen</translation>
    </message>
    <message>
        <location filename="../installtalkfrm.ui" line="82"/>
        <source>Change</source>
        <translation>Ändern</translation>
    </message>
    <message>
        <location filename="../installtalkfrm.ui" line="141"/>
        <source>Generate .talk files for Folders</source>
        <translation>.talk Dateien für Ordner erzeugen</translation>
    </message>
    <message>
        <location filename="../installtalkfrm.ui" line="151"/>
        <source>Generate .talk files for Files</source>
        <translation>.talk Dateien für Dateien erzeugen</translation>
    </message>
    <message>
        <location filename="../installtalkfrm.ui" line="202"/>
        <source>&amp;Install</source>
        <translation>&amp;Installieren</translation>
    </message>
</context>
<context>
    <name>InstallTalkWindow</name>
    <message>
        <location filename="../installtalkwindow.cpp" line="88"/>
        <source>The Folder to Talk is wrong!</source>
        <translation>Der Ordner für den Talk-Dateien erzeugt werden sollen ist falsch!</translation>
    </message>
    <message>
        <location filename="../installtalkwindow.cpp" line="119"/>
        <source>Selected TTS engine: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Gewähltes TTS-System: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../installtalkwindow.cpp" line="133"/>
        <source>Selected encoder: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Gewählter Encoder: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>PreviewFrm</name>
    <message>
        <location filename="../previewfrm.ui" line="16"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
</context>
<context>
    <name>ProgressLoggerFrm</name>
    <message>
        <location filename="../progressloggerfrm.ui" line="19"/>
        <source>Progress</source>
        <translation>Fortschritt</translation>
    </message>
    <message>
        <location filename="../progressloggerfrm.ui" line="68"/>
        <source>&amp;Abort</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../progressloggerfrm.ui" line="32"/>
        <source>progresswindow</source>
        <translation>Fortschrittsfenster</translation>
    </message>
</context>
<context>
    <name>ProgressLoggerGui</name>
    <message>
        <location filename="../progressloggergui.cpp" line="113"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../progressloggergui.cpp" line="95"/>
        <source>&amp;Abort</source>
        <translation>&amp;Abbrechen</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../base/detect.cpp" line="111"/>
        <source>Guest</source>
        <translation>Gast</translation>
    </message>
    <message>
        <location filename="../base/detect.cpp" line="114"/>
        <source>Admin</source>
        <translation>Admin</translation>
    </message>
    <message>
        <location filename="../base/detect.cpp" line="117"/>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location filename="../base/detect.cpp" line="120"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../base/detect.cpp" line="228"/>
        <source>(no description available)</source>
        <translation>(keine Beschreibung verfügbar)</translation>
    </message>
    <message>
        <location filename="../base/detect.cpp" line="400"/>
        <source>&lt;li&gt;Permissions insufficient for bootloader installation.
Administrator priviledges are necessary.&lt;/li&gt;</source>
        <translation>&lt;li&gt;Bereichtigung für Bootloader-Installation nicht ausreichend.
Administratorrechte sind notwendig.&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../base/detect.cpp" line="411"/>
        <source>&lt;li&gt;Target mismatch detected.
Installed target: %1, selected target: %2.&lt;/li&gt;</source>
        <translation>&lt;li&gt;Abweichendes Gerät erkannt.
Installiertes Gerät: %1, gewähltes Gerät: %2.&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../base/detect.cpp" line="416"/>
        <source>Problem detected:</source>
        <translation>Problem gefunden:</translation>
    </message>
</context>
<context>
    <name>RbUtilQt</name>
    <message>
        <location filename="../rbutilqt.cpp" line="251"/>
        <source>Network error: %1. Please check your network and proxy settings.</source>
        <translation>Netzwerkfehler: %1. Bitte Netzwerk und Proxyeinstellungen überprüfen.</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="352"/>
        <source>&lt;b&gt;%1 %2&lt;/b&gt; at &lt;b&gt;%3&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1 %2&lt;/b&gt; an &lt;b&gt;%3&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="371"/>
        <source>&lt;a href=&apos;%1&apos;&gt;PDF Manual&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%1&apos;&gt;PDF-Handbuch&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="373"/>
        <source>&lt;a href=&apos;%1&apos;&gt;HTML Manual (opens in browser)&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%1&apos;&gt;HTML-Handbuch (öffnet im Browser)&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="376"/>
        <source>Select a device for a link to the correct manual</source>
        <translation>Ein Gerät muss ausgewählt sein, damit ein Link zum entsprechenden Handbuch angezeigt wird</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="378"/>
        <source>&lt;a href=&apos;%1&apos;&gt;Manual Overview&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%1&apos;&gt;Anleitungen-Übersicht&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="887"/>
        <source>Confirm Installation</source>
        <translation>Installation bestätigen</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="622"/>
        <source>Do you really want to install the Bootloader?</source>
        <translation>Bootloader wirklich installieren?</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="800"/>
        <source>Do you really want to install the fonts package?</source>
        <translation>Schriftarten-Paket wirklich installieren?</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="849"/>
        <source>Do you really want to install the voice file?</source>
        <translation>Sprachdateien wirklich installieren?</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="888"/>
        <source>Do you really want to install the game addon files?</source>
        <translation>Zusatzdateien für Spiele wirklich installieren?</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="969"/>
        <source>Confirm Uninstallation</source>
        <translation>Entfernen bestätigen</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="970"/>
        <source>Do you really want to uninstall the Bootloader?</source>
        <translation>Bootloader wirklich entfernen?</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="1028"/>
        <source>Confirm download</source>
        <translation>Download bestätigen</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="1030"/>
        <source>Do you really want to download the manual? The manual will be saved to the root folder of your player.</source>
        <translation>Handbuch wirklich herunterladen? Das Handbuch wird im Wurzelordner des Geräts gespeichert.</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="1077"/>
        <source>Confirm installation</source>
        <translation>Installation bestätigen</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="1079"/>
        <source>Do you really want to install Rockbox Utility to your player? After installation you can run it from the players hard drive.</source>
        <translation>Rockbox Utility wirklich auf dem Gerät installieren? Nach der Installation kann es von dem Laufwerk des Geräts ausgeführt werden.</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="1087"/>
        <source>Installing Rockbox Utility</source>
        <translation>Installiere Rockbox Utility</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="1091"/>
        <source>Mount point is wrong!</source>
        <translation>Falscher Einhängepunkt!</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="1105"/>
        <source>Error installing Rockbox Utility</source>
        <translation>Fehler beim installieren von Rockbox Utility</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="1109"/>
        <source>Installing user configuration</source>
        <translation>Installiere Benutzerkonfiguration</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="1113"/>
        <source>Error installing user configuration</source>
        <translation>Fehler beim installieren der Benutzerkonfiguration</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="1117"/>
        <source>Successfully installed Rockbox Utility.</source>
        <translation>Rockbox Utility erfolgreich installiert.</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="84"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="84"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="1207"/>
        <source>Configuration error</source>
        <translation>Konfigurationsfehler</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="882"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="883"/>
        <source>Your device doesn&apos;t have a doom plugin. Aborting.</source>
        <translation>Für das gewählte Gerät existiert kein Doom-Plugin. Abbruch.</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="1209"/>
        <source>Your configuration is invalid. Please go to the configuration dialog and make sure the selected values are correct.</source>
        <translation>Die Konfiguration ist ungültig. Bitte im Konfigurationsdialog sicherstellen dass die Einstellungen korrekt sind.</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="215"/>
        <source>This is a new installation of Rockbox Utility, or a new version. The configuration dialog will now open to allow you to setup the program,  or review your settings.</source>
        <translation>Dies ist eine neue Installation oder eine neue Version von Rockbox Utility. Der Konfigurationsdialog wird nun automatisch geöffnet umd das Programm zu konfigurieren oder die Einstellungen zu prüfen.</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="539"/>
        <source>Aborted!</source>
        <translation>Abgebrochen!</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="548"/>
        <source>Installed Rockbox detected</source>
        <translation>Installiertes Rockbox erkannt</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="549"/>
        <source>Rockbox installation detected. Do you want to backup first?</source>
        <translation>Installiertes Rockbox erkannt. Soll zunächst eine Sicherungskopie gemacht werden?</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="574"/>
        <source>Backup failed!</source>
        <translation>Sicherung fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="842"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="844"/>
        <source>The Application is still downloading Information about new Builds. Please try again shortly.</source>
        <translation type="unfinished">Das Progamm lädt noch Informationen über neue Builds. Bitte in Kürze nochmals versuchen.</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="552"/>
        <source>Starting backup...</source>
        <translation>Erstelle Sicherungskopie ...</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="212"/>
        <source>New installation</source>
        <translation>Neue Installation</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="223"/>
        <source>Your configuration is invalid. This is most likely due to a changed device path. The configuration dialog will now open to allow you to correct the problem.</source>
        <translation>Die Konfiguration ist ungültig. Dies kommt wahrscheinlich von einem geänderten Gerätepfad. Der Konfigurationsdialog wird geöffnet, damit das Problem korrigiert werden kann.</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="570"/>
        <source>Backup successful</source>
        <translation>Sicherung erfolgreich</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="176"/>
        <source>Network error</source>
        <translation>Netzwerkfehler</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="177"/>
        <source>Can&apos;t get version information.</source>
        <translation>Konnte Versionsinformationen nicht laden.</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="536"/>
        <source>Really continue?</source>
        <translation>Wirklich fortfahren?</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="656"/>
        <source>No install method known.</source>
        <translation>Keine Installationsmethode bekannt.</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="683"/>
        <source>Bootloader detected</source>
        <translation>Bootloader erkannt</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="684"/>
        <source>Bootloader already installed. Do you want to reinstall the bootloader?</source>
        <translation>Bootloader ist bereits installiert. Soll der Bootloader neu installiert werden?</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="706"/>
        <source>Create Bootloader backup</source>
        <translation>Erzeuge Sicherungskopie vom Bootloader</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="711"/>
        <source>You can create a backup of the original bootloader file. Press &quot;Yes&quot; to select an output folder on your computer to save the file to. The file will get placed in a new folder &quot;%1&quot; created below the selected folder.
Press &quot;No&quot; to skip this step.</source>
        <translation>Es kann eine Sicherungskopie der originalen Bootloader-Datei erstellt werden. &quot;Ja&quot; um einen Zielordner auf dem Computer auszuwählen. Die Datei wird in einem neuen Unterordner &quot;%1&quot; im gewählten Ordner abgelegt.
&quot;Nein&quot; um diesen Schritt zu überspringen.</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="713"/>
        <source>Browse backup folder</source>
        <translation>Ordner für Sicherungskopie suchen</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="726"/>
        <source>Prerequisites</source>
        <translation>Voraussetzungen</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="739"/>
        <source>Select firmware file</source>
        <translation>Firmware-Datei auswählen</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="741"/>
        <source>Error opening firmware file</source>
        <translation>Fehler beim Öffnen der Firmware-Datei</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="759"/>
        <source>Backup error</source>
        <translation>Sicherungskopie-Fehler</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="760"/>
        <source>Could not create backup file. Continue?</source>
        <translation>Konnte Sicherungskopie-Datei nicht erzeugen. Fortfahren?</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="790"/>
        <source>Manual steps required</source>
        <translation>Manuelle Schritte erforderlich</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="998"/>
        <source>No uninstall method known.</source>
        <translation>Keine Deinstallationsmethode bekannt.</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="391"/>
        <source>Do you really want to perform a complete installation?

This will install Rockbox %1. To install the most recent development build available press &quot;Cancel&quot; and use the &quot;Installation&quot; tab.</source>
        <translation>Wirklich eine vollständige Installation durchführen?

Dies installiert Rockbox %1. Um die letzte Entwicklerversion zu installieren &quot;Abbrechen&quot; wählen und den Reiter &quot;Installation&quot; verwenden.</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="449"/>
        <source>Do you really want to perform a minimal installation? A minimal installation will contain only the absolutely necessary parts to run Rockbox.

This will install Rockbox %1. To install the most recent development build available press &quot;Cancel&quot; and use the &quot;Installation&quot; tab.</source>
        <translation>Wirklich eine Minimalinstallation durchführen? Eine Minimalinstallation enthält nur die Teile die zum Verwenden von Rockbox absolut notwendig sind.Dies installiert Rockbox %1. Um die letzte Entwicklerversion zu installieren &quot;Abbrechen&quot; wählen und den Reiter &quot;Installation&quot; verwenden.</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="689"/>
        <source>Bootloader installation skipped</source>
        <translation>Bootloader-Installation übersprungen</translation>
    </message>
    <message>
        <location filename="../rbutilqt.cpp" line="732"/>
        <source>Bootloader installation aborted</source>
        <translation>Bootloader-Installation abgebrochen</translation>
    </message>
</context>
<context>
    <name>RbUtilQtFrm</name>
    <message>
        <location filename="../rbutilqtfrm.ui" line="13"/>
        <source>Rockbox Utility</source>
        <translation>Rockbox Utility</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="822"/>
        <source>&amp;Quick Start</source>
        <translation>&amp;Schnellstart</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="815"/>
        <source>&amp;Installation</source>
        <translation>&amp;Installation</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="829"/>
        <source>&amp;Extras</source>
        <translation>&amp;Extras</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="845"/>
        <source>&amp;Uninstallation</source>
        <translation>Ent&amp;fernen</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="645"/>
        <source>&amp;Manual</source>
        <translation>&amp;Anleitung</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="792"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="886"/>
        <source>&amp;About</source>
        <translation>Ü&amp;ber</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="863"/>
        <source>Empty local download cache</source>
        <translation>Download-Cache löschen</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="868"/>
        <source>Install Rockbox Utility on player</source>
        <translation>Rockbox Utility auf dem Gerät installieren</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="873"/>
        <source>&amp;Configure</source>
        <translation>&amp;Konfigurieren</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="878"/>
        <source>E&amp;xit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="881"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished">Strg+Q</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="891"/>
        <source>About &amp;Qt</source>
        <translation>Über &amp;Qt</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="266"/>
        <source>Install Rockbox</source>
        <translation>Rockbox installieren</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="237"/>
        <source>Install Bootloader</source>
        <translation>Bootloader installieren</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="334"/>
        <source>Install Fonts package</source>
        <translation>Schriftarten-Paket installieren</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="360"/>
        <source>Install themes</source>
        <translation>Themes installieren</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="386"/>
        <source>Install game files</source>
        <translation>Spieldateien installieren</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="560"/>
        <source>Uninstall Bootloader</source>
        <translation>Bootloader entfernen</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="586"/>
        <source>Uninstall Rockbox</source>
        <translation>Rockbox entfernen</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="78"/>
        <source>Device</source>
        <translation>Gerät</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="90"/>
        <source>Selected device:</source>
        <translation>Ausgewähltes Gerät:</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="117"/>
        <source>&amp;Change</source>
        <translation type="unfinished">Ä&amp;ndern</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="137"/>
        <source>Welcome</source>
        <translation>Willkommen</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="231"/>
        <source>Basic Rockbox installation</source>
        <translation>Einfache Rockbox-Installation</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="328"/>
        <source>Install extras for Rockbox</source>
        <translation>Installiere Extras für Rockbox</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="837"/>
        <source>&amp;Accessibility</source>
        <translation type="unfinished">&amp;Zugänglichkeit</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="442"/>
        <source>Install accessibility add-ons</source>
        <translation>Installiere Zugänglichkeits-Erweiterungen</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="448"/>
        <source>Install Voice files</source>
        <translation>Sprachdateien installieren</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="474"/>
        <source>Install Talk files</source>
        <translation>Talk-Dateien installieren</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="648"/>
        <source>View and download the manual</source>
        <translation>Anleitung herunterladen und lesen</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="654"/>
        <source>Read the manual</source>
        <translation>Anleitung lesen</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="660"/>
        <source>PDF manual</source>
        <translation>PDF-Anleitung</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="673"/>
        <source>HTML manual</source>
        <translation>HTML-Anleitung</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="689"/>
        <source>Download the manual</source>
        <translation>Anleitung herunterladen</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="697"/>
        <source>&amp;PDF version</source>
        <translation>&amp;PDF-Version</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="704"/>
        <source>&amp;HTML version (zip file)</source>
        <translation>&amp;HTML-Version (Zip-Datei)</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="726"/>
        <source>Down&amp;load</source>
        <translation>Herunter&amp;laden</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="750"/>
        <source>Inf&amp;o</source>
        <translation>Inf&amp;o</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="770"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="896"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="143"/>
        <source>Complete Installation</source>
        <translation>Komplette Installation</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="811"/>
        <source>Action&amp;s</source>
        <translation>A&amp;ktionen</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="901"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="994"/>
        <source>Read PDF manual</source>
        <translation>Lese Anleitung im PDF-Format</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="999"/>
        <source>Read HTML manual</source>
        <translation>Lese Anleitung im HTML-Format</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="1004"/>
        <source>Download PDF manual</source>
        <translation>Lade Anleitung im PDF-Format herunter</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="1009"/>
        <source>Download HTML manual (zip)</source>
        <translation>Lade Anleitung im HTML-Format herunter</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="523"/>
        <source>Create Voice files</source>
        <translation>Erstelle Sprachdateien</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="1020"/>
        <source>Create Voice File</source>
        <translation>Erstelle Sprachdatei</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="159"/>
        <source>&lt;b&gt;Complete Installation&lt;/b&gt;&lt;br/&gt;This installs the bootloader, a current build and the extras package. This is the recommended method for new installations.</source>
        <translation type="unfinished">&lt;b&gt;Komplette Installation&lt;/b&gt;&lt;br/&gt;Dies installiert den Bootloader, ein aktuellen Build und die Extra-Pakete. Dies ist die empfohlene Methode für eine neue Installation.</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="253"/>
        <source>&lt;b&gt;Install the bootloader&lt;/b&gt;&lt;br/&gt;Before Rockbox can be run on your audio player, you may have to install a bootloader. This is only necessary the first time Rockbox is installed.</source>
        <translation>&lt;b&gt;Bootloader installieren&lt;/b&gt;&lt;br/&gt;Bevor Rockbox auf dem Gerät läuft muss der Bootloader installiert werden. Dies ist nur bei der ersten Installation notwendig.</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="282"/>
        <source>&lt;b&gt;Install Rockbox&lt;/b&gt; on your audio player</source>
        <translation>&lt;b&gt;Installiere Rockbox&lt;/b&gt; auf dem Gerät</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="350"/>
        <source>&lt;b&gt;Fonts Package&lt;/b&gt;&lt;br/&gt;The Fonts Package contains a couple of commonly used fonts. Installation is highly recommended.</source>
        <translation>&lt;b&gt;Installiere Schriften&lt;/b&gt;&lt;br/&gt;Das Schriftenpaket enthält eine Reihe von häufig benutzen Schriften. Die Installation wird empfohlen.</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="376"/>
        <source>&lt;b&gt;Install Themes&lt;/b&gt;&lt;br/&gt;Rockbox&apos; look can be customized by themes. You can choose and install several officially distributed themes.</source>
        <translation>&lt;b&gt;Installiere Themes&lt;/b&gt;&lt;br/&gt;Das Aussehen von Rockbox kann mit Themes verändert werden. Es stehen verschiedene offizielle Themen zur Auswahl.</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="402"/>
        <source>&lt;b&gt;Install Game Files&lt;/b&gt;&lt;br/&gt;Doom needs a base wad file to run.</source>
        <translation>&lt;b&gt;Installiere Spiele-Dateien&lt;/b&gt;&lt;br/&gt;Doom benötigt eine &quot;base wad&quot;-Datei um zu funktionieren.</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="464"/>
        <source>&lt;b&gt;Install Voice file&lt;/b&gt;&lt;br/&gt;Voice files are needed to make Rockbox speak the user interface. Speaking is enabled by default, so if you installed the voice file Rockbox will speak.</source>
        <translation>&lt;b&gt;Installiere Sprachdatei&lt;/b&gt;&lt;br&gt;Sprachdateien werden benötigt, damit Rockbox die Menüs vorlesen kann. Sprachausgabe ist standardmäßig angeschaltet. Sobald eine Sprachdatei installiert ist, werden die Menüs gesprochen.</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="490"/>
        <source>&lt;b&gt;Create Talk Files&lt;/b&gt;&lt;br/&gt;Talkfiles are needed to let Rockbox speak File and Foldernames</source>
        <translation>&lt;b&gt;Erstelle Talk Dateien&lt;/b&gt;&lt;br/&gt;Talkdateien werden benötigt, damit Rockbox Dateien und Ordner vorlesen kann</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="539"/>
        <source>&lt;b&gt;Create Voice file&lt;/b&gt;&lt;br/&gt;Voice files are needed to make Rockbox speak the  user interface. Speaking is enabled by default, so
 if you installed the voice file Rockbox will speak.</source>
        <translation>&lt;b&gt;Erzeuge Sprachdatei&lt;/b&gt;&lt;br&gt; Sprachdateien werden benötigt, damit Rockbox seine Benutzeroberfläche vorlesen kann. Sprachausgabe ist Standardmäßig angeschaltet, sobald sie eine Sprachdatei installieren wird Rockbox sprechen.</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="576"/>
        <source>&lt;b&gt;Remove the bootloader&lt;/b&gt;&lt;br/&gt;After removing the bootloader you won&apos;t be able to start Rockbox.</source>
        <translation>&lt;b&gt;Entferne Bootloader&lt;/b&gt;&lt;br/&gt;Nach dem Entfernen des Bootloaders kann Rockbox nicht mehr gestartet werden.</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="602"/>
        <source>&lt;b&gt;Uninstall Rockbox from your audio player.&lt;/b&gt;&lt;br/&gt;This will leave the bootloader in place (you need to remove it manually).</source>
        <translation>&lt;b&gt;Entferne Rockbox vom Gerät&lt;/b&gt;&lt;br/&gt;Dies wird den Bootloader intakt lassen (er muss manuell entfernt werden).</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="756"/>
        <source>Currently installed packages.&lt;br/&gt;&lt;b&gt;Note:&lt;/b&gt; if you manually installed packages this might not be correct!</source>
        <translation type="unfinished">Aktuell installierte Pakete.&lt;br/&gt;&lt;b&gt;Hinweis:&lt;/b&gt; wenn Pakete manuell installiert wurden wird diese Anzeige nicht korrekt sein!</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="801"/>
        <source>Abou&amp;t</source>
        <translation>Ü&amp;ber</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="925"/>
        <source>Install &amp;Bootloader</source>
        <translation>Installiere &amp;Bootloader</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="933"/>
        <source>Install &amp;Rockbox</source>
        <translation>Installiere &amp;Rockbox</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="941"/>
        <source>Install &amp;Fonts Package</source>
        <translation>Installiere &amp;Schriften-Paket</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="949"/>
        <source>Install &amp;Themes</source>
        <translation>Installiere &amp;Themen</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="957"/>
        <source>Install &amp;Game Files</source>
        <translation>Installiere &amp;Spiele-Dateien</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="965"/>
        <source>&amp;Install Voice File</source>
        <translation>&amp;Installiere Sprachdateien</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="973"/>
        <source>Create &amp;Talk Files</source>
        <translation>Erstelle &amp;Talk-Dateien</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="981"/>
        <source>Remove &amp;bootloader</source>
        <translation>&amp;Bootloader entfernen</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="989"/>
        <source>Uninstall &amp;Rockbox</source>
        <translation>&amp;Rockbox entfernen</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="1017"/>
        <source>Create &amp;Voice File</source>
        <translation>&amp;Sprachdateien erzeugen</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="1025"/>
        <source>&amp;System Info</source>
        <translation>&amp;Systeminfo</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="909"/>
        <source>&amp;Complete Installation</source>
        <translation type="unfinished">&amp;Vollständige Installation</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="97"/>
        <source>device / mountpoint unknown or invalid</source>
        <translation>Gerär / Einhängepunkt unbekannt oder ungültig</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="172"/>
        <source>Minimal Installation</source>
        <translation>Minimale Installation</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="188"/>
        <source>&lt;b&gt;Minimal installation&lt;/b&gt;&lt;br/&gt;This installs bootloader and the current build of Rockbox. If you don&apos;t want the extras package, choose this option.</source>
        <translation>&lt;b&gt;Minimale Installation&lt;/b&gt;&lt;br/&gt;Dies installiert Bootloader und die aktuelle Version von Rockbox. Diese Option verwenden wenn keine Zusatzpakete gewünscht werden.</translation>
    </message>
    <message>
        <location filename="../rbutilqtfrm.ui" line="917"/>
        <source>&amp;Minimal Installation</source>
        <translation>&amp;Minimale Installation</translation>
    </message>
</context>
<context>
    <name>Sysinfo</name>
    <message>
        <location filename="../sysinfo.cpp" line="41"/>
        <source>&lt;b&gt;OS&lt;/b&gt;&lt;br/&gt;</source>
        <translation>&lt;b&gt;Betriebssystem&lt;/b&gt;&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../sysinfo.cpp" line="42"/>
        <source>&lt;b&gt;Username&lt;/b&gt;&lt;br/&gt;%1&lt;hr/&gt;</source>
        <translation>&lt;b&gt;Benutzername&lt;/b&gt;&lt;br/&gt;%1&lt;hr/&gt;</translation>
    </message>
    <message>
        <location filename="../sysinfo.cpp" line="44"/>
        <source>&lt;b&gt;Permissions&lt;/b&gt;&lt;br/&gt;%1&lt;hr/&gt;</source>
        <translation>&lt;b&gt;Berechtigungen&lt;/b&gt;&lt;br/&gt;%1&lt;hr/&gt;</translation>
    </message>
    <message>
        <location filename="../sysinfo.cpp" line="46"/>
        <source>&lt;b&gt;Attached USB devices&lt;/b&gt;&lt;br/&gt;</source>
        <translation>&lt;b&gt;Angeschlossene USB-Geräte&lt;/b&gt;&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../sysinfo.cpp" line="51"/>
        <source>VID: %1 PID: %2, %3</source>
        <translation>VID: %1 PID: %2, %3</translation>
    </message>
    <message>
        <location filename="../sysinfo.cpp" line="59"/>
        <source>Filesystem</source>
        <translation>Dateisystem</translation>
    </message>
    <message>
        <location filename="../sysinfo.cpp" line="63"/>
        <source>%1, %2 MiB available</source>
        <translation>%1, %2 MiB verfügbar</translation>
    </message>
</context>
<context>
    <name>SysinfoFrm</name>
    <message>
        <location filename="../sysinfofrm.ui" line="13"/>
        <source>System Info</source>
        <translation>Systeminfo</translation>
    </message>
    <message>
        <location filename="../sysinfofrm.ui" line="22"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Aktualisieren</translation>
    </message>
    <message>
        <location filename="../sysinfofrm.ui" line="45"/>
        <source>&amp;OK</source>
        <translation>&amp;Ok</translation>
    </message>
</context>
<context>
    <name>TTSExes</name>
    <message>
        <location filename="../base/tts.cpp" line="139"/>
        <source>TTS executable not found</source>
        <translation>TTS-System nicht gefunden</translation>
    </message>
    <message>
        <location filename="../base/tts.cpp" line="113"/>
        <source>Path to TTS engine:</source>
        <translation>Pfad zum TTS-System:</translation>
    </message>
    <message>
        <location filename="../base/tts.cpp" line="115"/>
        <source>TTS engine options:</source>
        <translation>TTS-System Optionen:</translation>
    </message>
</context>
<context>
    <name>TTSFestival</name>
    <message>
        <location filename="../base/tts.cpp" line="491"/>
        <source>engine could not voice string</source>
        <translation type="unfinished">Konnte String nicht sprechen</translation>
    </message>
    <message>
        <location filename="../base/tts.cpp" line="561"/>
        <source>No description available</source>
        <translation>keine Beschreibung verfügbar</translation>
    </message>
    <message>
        <location filename="../base/tts.cpp" line="381"/>
        <source>Path to Festival client:</source>
        <translation>Pfad zu Festival-Client:</translation>
    </message>
    <message>
        <location filename="../base/tts.cpp" line="385"/>
        <source>Voice:</source>
        <translation>Stimme:</translation>
    </message>
    <message>
        <location filename="../base/tts.cpp" line="392"/>
        <source>Voice description:</source>
        <translation>Stimmenbeschreibung:</translation>
    </message>
</context>
<context>
    <name>TTSSapi</name>
    <message>
        <location filename="../base/tts.cpp" line="238"/>
        <source>Could not copy the Sapi-script</source>
        <translation>Konnte Sapi-Skript nicht kopieren</translation>
    </message>
    <message>
        <location filename="../base/tts.cpp" line="259"/>
        <source>Could not start the Sapi-script</source>
        <translation>Konnte Sapi-Skript nicht starten</translation>
    </message>
    <message>
        <location filename="../base/tts.cpp" line="185"/>
        <source>Language:</source>
        <translation>Sprache:</translation>
    </message>
    <message>
        <location filename="../base/tts.cpp" line="190"/>
        <source>Voice:</source>
        <translation>Stimme:</translation>
    </message>
    <message>
        <location filename="../base/tts.cpp" line="195"/>
        <source>Speed:</source>
        <translation>Geschwindigkeit:</translation>
    </message>
    <message>
        <location filename="../base/tts.cpp" line="198"/>
        <source>Options:</source>
        <translation>Optionen:</translation>
    </message>
</context>
<context>
    <name>TalkFileCreator</name>
    <message>
        <location filename="../talkfile.cpp" line="41"/>
        <source>Starting Talk file generation</source>
        <translation>Beginne Talkdatei-Erzeugung</translation>
    </message>
    <message>
        <location filename="../talkfile.cpp" line="49"/>
        <source>Init of TTS engine failed</source>
        <translation>Initalisierung des TTS-Systems fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../talkfile.cpp" line="59"/>
        <source>Init of Encoder engine failed</source>
        <translation>Initalisierung des Encoders fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../talkfile.cpp" line="392"/>
        <source>Talk file creation aborted</source>
        <translation>Erzeugen der Sprachdatei abgebrochen</translation>
    </message>
    <message>
        <location filename="../talkfile.cpp" line="312"/>
        <source>Encoding of %1 failed</source>
        <translation>Kodieren of %1 ist fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../talkfile.cpp" line="146"/>
        <source>Finished creating Talk files</source>
        <translation>Erstellen der Sprachdateien beendet</translation>
    </message>
    <message>
        <location filename="../talkfile.cpp" line="71"/>
        <source>Reading Filelist...</source>
        <translation>Lese Dateiliste ...</translation>
    </message>
    <message>
        <location filename="../talkfile.cpp" line="106"/>
        <source>Voicing entries...</source>
        <translation>Spreche Einträge ...</translation>
    </message>
    <message>
        <location filename="../talkfile.cpp" line="115"/>
        <source>Encoding files...</source>
        <translation>Kodiere Dateien ...</translation>
    </message>
    <message>
        <location filename="../talkfile.cpp" line="123"/>
        <source>Copying Talkfile for Dirs...</source>
        <translation>Kopiere Sprachateien für Ordner ...</translation>
    </message>
    <message>
        <location filename="../talkfile.cpp" line="132"/>
        <source>Copying Talkfile for Files...</source>
        <translation>Kopiere Sprachdateien für Dateien ...</translation>
    </message>
    <message>
        <location filename="../talkfile.cpp" line="416"/>
        <source>Copying of %1 to %2 failed</source>
        <translation>Kopieren von %1 nach %2 fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../talkfile.cpp" line="438"/>
        <source>Cleaning up..</source>
        <translation>Räume auf ...</translation>
    </message>
    <message>
        <location filename="../talkfile.cpp" line="274"/>
        <source>Voicing of %1 failed: %2</source>
        <translation type="unfinished">Sprechen von %1 fehlgeschlagen: %2</translation>
    </message>
</context>
<context>
    <name>ThemeInstallFrm</name>
    <message>
        <location filename="../themesinstallfrm.ui" line="13"/>
        <source>Theme Installation</source>
        <translation>Theme-Installation</translation>
    </message>
    <message>
        <location filename="../themesinstallfrm.ui" line="48"/>
        <source>Selected Theme</source>
        <translation>Ausgewähltes Theme</translation>
    </message>
    <message>
        <location filename="../themesinstallfrm.ui" line="73"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../themesinstallfrm.ui" line="83"/>
        <source>Download size:</source>
        <translation>Downloadgröße:</translation>
    </message>
    <message>
        <location filename="../themesinstallfrm.ui" line="125"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../themesinstallfrm.ui" line="115"/>
        <source>&amp;Install</source>
        <translation>&amp;Installieren</translation>
    </message>
    <message>
        <location filename="../themesinstallfrm.ui" line="93"/>
        <source>Hold Ctrl to select multiple item, Shift for a range</source>
        <translation>Strg halten um mehrer Einträge, Umschalt einen Bereich auszuwählen</translation>
    </message>
</context>
<context>
    <name>ThemesInstallWindow</name>
    <message>
        <location filename="../themesinstallwindow.cpp" line="36"/>
        <source>no theme selected</source>
        <translation>Kein Theme ausgewählt</translation>
    </message>
    <message>
        <location filename="../themesinstallwindow.cpp" line="103"/>
        <source>Network error: %1.
Please check your network and proxy settings.</source>
        <translation>Netzwerkfehler: %1
Bitte Netzwerk- und Proxyeinstellungen überprüfen.</translation>
    </message>
    <message>
        <location filename="../themesinstallwindow.cpp" line="115"/>
        <source>the following error occured:
%1</source>
        <translation>Der folgende Fehler ist aufgetreten:
%1</translation>
    </message>
    <message>
        <location filename="../themesinstallwindow.cpp" line="120"/>
        <source>done.</source>
        <translation>Abgeschlossen.</translation>
    </message>
    <message>
        <location filename="../themesinstallwindow.cpp" line="188"/>
        <source>fetching details for %1</source>
        <translation>lade Details für %1</translation>
    </message>
    <message>
        <location filename="../themesinstallwindow.cpp" line="190"/>
        <source>fetching preview ...</source>
        <translation>lade Vorschau ...</translation>
    </message>
    <message>
        <location filename="../themesinstallwindow.cpp" line="203"/>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;hr/&gt;</source>
        <translation>&lt;b&gt;Autor:&lt;/b&gt; %1&lt;hr/&gt;</translation>
    </message>
    <message>
        <location filename="../themesinstallwindow.cpp" line="206"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message>
        <location filename="../themesinstallwindow.cpp" line="205"/>
        <source>&lt;b&gt;Version:&lt;/b&gt; %1&lt;hr/&gt;</source>
        <translation>&lt;b&gt;Version:&lt;/b&gt; %1&lt;hr/&gt;</translation>
    </message>
    <message>
        <location filename="../themesinstallwindow.cpp" line="208"/>
        <source>no description</source>
        <translation>Keine Beschreibung vorhanden</translation>
    </message>
    <message>
        <location filename="../themesinstallwindow.cpp" line="251"/>
        <source>no theme preview</source>
        <translation>Keine Themevorschau vorhanden</translation>
    </message>
    <message>
        <location filename="../themesinstallwindow.cpp" line="282"/>
        <source>getting themes information ...</source>
        <translation>lade Theme-Informationen ...</translation>
    </message>
    <message>
        <location filename="../themesinstallwindow.cpp" line="330"/>
        <source>Mount point is wrong!</source>
        <translation>Einhängepunkt ungültig!</translation>
    </message>
    <message>
        <location filename="../themesinstallwindow.cpp" line="207"/>
        <source>&lt;b&gt;Description:&lt;/b&gt; %1&lt;hr/&gt;</source>
        <translation>&lt;b&gt;Beschreibung:&lt;/b&gt; %1&lt;hr/&gt;</translation>
    </message>
    <message>
        <location filename="../themesinstallwindow.cpp" line="37"/>
        <source>no selection</source>
        <translation>keine Auswahl</translation>
    </message>
    <message>
        <location filename="../themesinstallwindow.cpp" line="157"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message numerus="yes">
        <location filename="../themesinstallwindow.cpp" line="174"/>
        <source>Download size %L1 kiB (%n item(s))</source>
        <translation>
            <numerusform>Download-Größe %L1 kiB (%n Element)</numerusform>
            <numerusform>Download-Größe %L1 kiB (%n Elemente)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../themesinstallwindow.cpp" line="240"/>
        <source>Retrieving theme preview failed.
HTTP response code: %1</source>
        <translation>Laden der Vorschau fehlgeschlagen.
HTTP Antwortcode: %1</translation>
    </message>
</context>
<context>
    <name>UnZip</name>
    <message>
        <location filename="../zip/unzip.cpp" line="250"/>
        <source>ZIP operation completed successfully.</source>
        <translation>ZIP-Operation erfolgreich abgeschlossen.</translation>
    </message>
    <message>
        <location filename="../zip/unzip.cpp" line="251"/>
        <source>Failed to initialize or load zlib library.</source>
        <translation>Initialisieren oder Laden der zlib-Bibliothek fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../zip/unzip.cpp" line="252"/>
        <source>zlib library error.</source>
        <translation>Fehler in zlib-Bibliothek.</translation>
    </message>
    <message>
        <location filename="../zip/unzip.cpp" line="253"/>
        <source>Unable to create or open file.</source>
        <translation>Erzeugen oder Öffnen der Datei fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../zip/unzip.cpp" line="254"/>
        <source>Partially corrupted archive. Some files might be extracted.</source>
        <translation>Teilweise korruptes Archiv. Einige Dateien wurden möglicherweise extrahiert.</translation>
    </message>
    <message>
        <location filename="../zip/unzip.cpp" line="255"/>
        <source>Corrupted archive.</source>
        <translation>Korruptes Archiv.</translation>
    </message>
    <message>
        <location filename="../zip/unzip.cpp" line="256"/>
        <source>Wrong password.</source>
        <translation>Falsches Passwort.</translation>
    </message>
    <message>
        <location filename="../zip/unzip.cpp" line="257"/>
        <source>No archive has been created yet.</source>
        <translation>Momentan kein Archiv verfügbar.</translation>
    </message>
    <message>
        <location filename="../zip/unzip.cpp" line="258"/>
        <source>File or directory does not exist.</source>
        <translation>Datei oder Ordner existiert nicht.</translation>
    </message>
    <message>
        <location filename="../zip/unzip.cpp" line="259"/>
        <source>File read error.</source>
        <translation>Fehler beim Lesen der Datei.</translation>
    </message>
    <message>
        <location filename="../zip/unzip.cpp" line="260"/>
        <source>File write error.</source>
        <translation>Fehler beim Schreiben der Datei.</translation>
    </message>
    <message>
        <location filename="../zip/unzip.cpp" line="261"/>
        <source>File seek error.</source>
        <translation>Fehler beim Durchsuchen der Datei.</translation>
    </message>
    <message>
        <location filename="../zip/unzip.cpp" line="262"/>
        <source>Unable to create a directory.</source>
        <translation>Kann Verzeichnis nicht erstellen.</translation>
    </message>
    <message>
        <location filename="../zip/unzip.cpp" line="263"/>
        <source>Invalid device.</source>
        <translation>Ungültiges Gerät.</translation>
    </message>
    <message>
        <location filename="../zip/unzip.cpp" line="264"/>
        <source>Invalid or incompatible zip archive.</source>
        <translation>Ungültiges oder inkompatibles Zip-Archiv.</translation>
    </message>
    <message>
        <location filename="../zip/unzip.cpp" line="265"/>
        <source>Inconsistent headers. Archive might be corrupted.</source>
        <translation>Inkonsistente Header. Archiv ist möglicherweise beschädigt.</translation>
    </message>
    <message>
        <location filename="../zip/unzip.cpp" line="269"/>
        <source>Unknown error.</source>
        <translation>Unbekannter Fehler.</translation>
    </message>
</context>
<context>
    <name>UninstallFrm</name>
    <message>
        <location filename="../uninstallfrm.ui" line="16"/>
        <source>Uninstall Rockbox</source>
        <translation>Rockbox entfernen</translation>
    </message>
    <message>
        <location filename="../uninstallfrm.ui" line="35"/>
        <source>Please select the Uninstallation Method</source>
        <translation>Bitte Methode zum Entfernen auswählen</translation>
    </message>
    <message>
        <location filename="../uninstallfrm.ui" line="45"/>
        <source>Uninstallation Method</source>
        <translation>Methode zum Entfernen</translation>
    </message>
    <message>
        <location filename="../uninstallfrm.ui" line="51"/>
        <source>Complete Uninstallation</source>
        <translation>Vollständiges Entfernen</translation>
    </message>
    <message>
        <location filename="../uninstallfrm.ui" line="58"/>
        <source>Smart Uninstallation</source>
        <translation>Intelligente Entfernen</translation>
    </message>
    <message>
        <location filename="../uninstallfrm.ui" line="68"/>
        <source>Please select what you want to uninstall</source>
        <translation>Bitte die zu entfernenden Teile auswählen</translation>
    </message>
    <message>
        <location filename="../uninstallfrm.ui" line="78"/>
        <source>Installed Parts</source>
        <translation>Installierte Teile</translation>
    </message>
    <message>
        <location filename="../uninstallfrm.ui" line="138"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../uninstallfrm.ui" line="128"/>
        <source>&amp;Uninstall</source>
        <translation>&amp;Entfernen</translation>
    </message>
</context>
<context>
    <name>Uninstaller</name>
    <message>
        <location filename="../base/uninstall.cpp" line="46"/>
        <source>Starting Uninstallation</source>
        <translation>Beginne Entfernen</translation>
    </message>
    <message>
        <location filename="../base/uninstall.cpp" line="38"/>
        <source>Finished Uninstallation</source>
        <translation>Entfernen erfolgreich</translation>
    </message>
    <message>
        <location filename="../base/uninstall.cpp" line="52"/>
        <source>Uninstalling </source>
        <translation>Entferne  </translation>
    </message>
    <message>
        <location filename="../base/uninstall.cpp" line="83"/>
        <source>Could not delete: </source>
        <translation>Konnte Datei nicht löschen: </translation>
    </message>
    <message>
        <location filename="../base/uninstall.cpp" line="112"/>
        <source>Uninstallation finished</source>
        <translation>Entfernen erfolgreich</translation>
    </message>
</context>
<context>
    <name>VoiceFileCreator</name>
    <message>
        <location filename="../voicefile.cpp" line="43"/>
        <source>Starting Voicefile generation</source>
        <translation>Erzeugen der Sprachdatei beginnt</translation>
    </message>
    <message>
        <location filename="../voicefile.cpp" line="104"/>
        <source>Download error: received HTTP error %1.</source>
        <translation>Fehler beim Herunterladen: HTTP Fehler %1.</translation>
    </message>
    <message>
        <location filename="../voicefile.cpp" line="109"/>
        <source>Cached file used.</source>
        <translation>Datei aus Cache verwendet.</translation>
    </message>
    <message>
        <location filename="../voicefile.cpp" line="111"/>
        <source>Download error: %1</source>
        <translation>Downloadfehler: %1</translation>
    </message>
    <message>
        <location filename="../voicefile.cpp" line="116"/>
        <source>Download finished.</source>
        <translation>Download abgeschlossen.</translation>
    </message>
    <message>
        <location filename="../voicefile.cpp" line="125"/>
        <source>failed to open downloaded file</source>
        <translation>Konnte heruntergeladene Datei nicht öffnen</translation>
    </message>
    <message>
        <location filename="../voicefile.cpp" line="138"/>
        <source>Init of TTS engine failed</source>
        <translation>Initalisierung des TTS-Systems fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../voicefile.cpp" line="149"/>
        <source>Init of Encoder engine failed</source>
        <translation>Initalisierung des Encoder fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../voicefile.cpp" line="192"/>
        <source>The downloaded file was empty!</source>
        <translation>Die heruntergeladene Datei war leer!</translation>
    </message>
    <message>
        <location filename="../voicefile.cpp" line="231"/>
        <source>creating </source>
        <translation>erzeuge </translation>
    </message>
    <message>
        <location filename="../voicefile.cpp" line="257"/>
        <source>Error opening downloaded file</source>
        <translation>Konnte heruntergeladene Datei nicht öffnen</translation>
    </message>
    <message>
        <location filename="../voicefile.cpp" line="266"/>
        <source>Error opening output file</source>
        <translation>Konnte Ausgabedatei nicht öffnen</translation>
    </message>
    <message>
        <location filename="../voicefile.cpp" line="288"/>
        <source>successfully created.</source>
        <translation>erfolgreich erzeugt.</translation>
    </message>
    <message>
        <location filename="../voicefile.cpp" line="57"/>
        <source>could not find rockbox-info.txt</source>
        <translation>Konnte rockbox-info.txt nicht finden</translation>
    </message>
</context>
<context>
    <name>Zip</name>
    <message>
        <location filename="../zip/zip.cpp" line="481"/>
        <source>ZIP operation completed successfully.</source>
        <translation>ZIP-Vorgang erfolgreich abgeschlossen.</translation>
    </message>
    <message>
        <location filename="../zip/zip.cpp" line="482"/>
        <source>Failed to initialize or load zlib library.</source>
        <translation>Initialisieren oder Laden der zlib-Bibliothek fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../zip/zip.cpp" line="483"/>
        <source>zlib library error.</source>
        <translation>Fehler in zlib-Bibliothek.</translation>
    </message>
    <message>
        <location filename="../zip/zip.cpp" line="484"/>
        <source>Unable to create or open file.</source>
        <translation>Erzeugen oder Öffnen der Datei fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../zip/zip.cpp" line="485"/>
        <source>No archive has been created yet.</source>
        <translation>Noch kein Archiv erzeugt.</translation>
    </message>
    <message>
        <location filename="../zip/zip.cpp" line="486"/>
        <source>File or directory does not exist.</source>
        <translation>Datei oder Ordner existiert nicht.</translation>
    </message>
    <message>
        <location filename="../zip/zip.cpp" line="487"/>
        <source>File read error.</source>
        <translation>Fehler beim Lesen der Datei.</translation>
    </message>
    <message>
        <location filename="../zip/zip.cpp" line="488"/>
        <source>File write error.</source>
        <translation>Fehler beim Schreiben der Datei.</translation>
    </message>
    <message>
        <location filename="../zip/zip.cpp" line="489"/>
        <source>File seek error.</source>
        <translation>Fehler beim Durchsuchen der Datei.</translation>
    </message>
    <message>
        <location filename="../zip/zip.cpp" line="493"/>
        <source>Unknown error.</source>
        <translation>Unbekannter Fehler.</translation>
    </message>
</context>
<context>
    <name>ZipInstaller</name>
    <message>
        <location filename="../base/zipinstaller.cpp" line="59"/>
        <source>done.</source>
        <translation>Abgeschlossen.</translation>
    </message>
    <message>
        <location filename="../base/zipinstaller.cpp" line="67"/>
        <source>Installation finished successfully.</source>
        <translation>Installation erfolgreich abgeschlossen.</translation>
    </message>
    <message>
        <location filename="../base/zipinstaller.cpp" line="80"/>
        <source>Downloading file %1.%2</source>
        <translation>Herunterladen von Datei %1.%2</translation>
    </message>
    <message>
        <location filename="../base/zipinstaller.cpp" line="115"/>
        <source>Download error: received HTTP error %1.</source>
        <translation>Fehler beim Herunterladen: HTTP Fehler %1.</translation>
    </message>
    <message>
        <location filename="../base/zipinstaller.cpp" line="122"/>
        <source>Download error: %1</source>
        <translation>Downloadfehler: %1</translation>
    </message>
    <message>
        <location filename="../base/zipinstaller.cpp" line="126"/>
        <source>Download finished.</source>
        <translation>Download abgeschlossen.</translation>
    </message>
    <message>
        <location filename="../base/zipinstaller.cpp" line="132"/>
        <source>Extracting file.</source>
        <translation>Extrahiere Datei.</translation>
    </message>
    <message>
        <location filename="../base/zipinstaller.cpp" line="142"/>
        <source>Opening archive failed: %1.</source>
        <translation>Öffnen des Archives fehlgeschlagen: %1.</translation>
    </message>
    <message>
        <location filename="../base/zipinstaller.cpp" line="161"/>
        <source>Extracting failed: %1.</source>
        <translation>Extrahieren fehlgeschlagen: %1.</translation>
    </message>
    <message>
        <location filename="../base/zipinstaller.cpp" line="171"/>
        <source>Installing file.</source>
        <translation>Installiere Datei.</translation>
    </message>
    <message>
        <location filename="../base/zipinstaller.cpp" line="182"/>
        <source>Installing file failed.</source>
        <translation>Dateiinstallation fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../base/zipinstaller.cpp" line="191"/>
        <source>Creating installation log</source>
        <translation>Erstelle Installationslog</translation>
    </message>
    <message>
        <location filename="../base/zipinstaller.cpp" line="120"/>
        <source>Cached file used.</source>
        <translation>Datei aus Cache verwendet.</translation>
    </message>
    <message>
        <location filename="../base/zipinstaller.cpp" line="152"/>
        <source>Not enough disk space! Aborting.</source>
        <translation>Nicht genügend Speicherplatz verfügbar! Abbruch.</translation>
    </message>
</context>
<context>
    <name>aboutBox</name>
    <message>
        <location filename="../aboutbox.ui" line="13"/>
        <source>About Rockbox Utility</source>
        <translation>Über Rockbox Utility</translation>
    </message>
    <message>
        <location filename="../aboutbox.ui" line="95"/>
        <source>&amp;Credits</source>
        <translation>&amp;Credits</translation>
    </message>
    <message>
        <location filename="../aboutbox.ui" line="127"/>
        <source>&amp;License</source>
        <translation>&amp;Lizenz</translation>
    </message>
    <message>
        <location filename="../aboutbox.ui" line="169"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../aboutbox.ui" line="34"/>
        <source>The Rockbox Utility</source>
        <translation>Rockbox Utility</translation>
    </message>
    <message>
        <location filename="../aboutbox.ui" line="72"/>
        <source>http://www.rockbox.org</source>
        <translation>http://www.rockbox.org</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../aboutbox.ui" line="56"/>
        <source>Installer and housekeeping utility for the Rockbox open source digital audio player firmware.

© 2005 - 2009 The Rockbox Team.
Released under the GNU General Public License v2.</source>
        <translation>Installations- und Wartungstool für die Open-Source-Firmware für digitale Audioabspieler Rockbox.

© 2005 - 2009 Das Rockbox Team.
Veröffentlicht unter der GNU General Public License v2.</translation>
    </message>
</context>
</TS>
